import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.IOUtils;

public class Main {

    public static void main(String[] args) throws IOException {
        String filename = "init.txt";
        sortAsc(filename);
        sortDesc(filename);

    }

    private static void sortDesc(String filename) throws IOException {
        System.out.println("Desc:");
        String[] text = openFile(filename);
        Integer[] intArray = toInteger(text);
        Arrays.sort(intArray, Collections.reverseOrder());
        for (int s : intArray) {
            System.out.print(s + "  ");
        }
        System.out.println("\n-------------------------------");
    }

    private static void sortAsc(String filename) throws IOException {
        System.out.println("Asc:");
        String[] text = openFile(filename);
        Integer[] intArray = toInteger(text);
        Arrays.sort(intArray);
        for (int s : intArray) {
            System.out.print(s + "  ");
        }
        System.out.println("\n-------------------------------");
    }

    private static String[] openFile(String filename) throws IOException {
        ClassLoader classLoader = Main.class.getClassLoader();
        try (InputStream inputStream = classLoader.getResourceAsStream(filename)) {
            assert inputStream != null;
            return IOUtils.toString(inputStream, StandardCharsets.UTF_8).split(",");
        } catch (IOException e) {
            throw new IOException("Can not reads the file");
        }
    }

    private static Integer[] toInteger(String[] strings) {
        Integer[] intRes = new Integer[strings.length];
        for (int i = 0; i < strings.length; i++) {
            intRes[i] = Integer.valueOf(strings[i]);
        }
        return intRes;
    }

}
